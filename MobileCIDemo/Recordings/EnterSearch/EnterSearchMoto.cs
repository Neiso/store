﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace MobileCIDemo.Recordings.EnterSearch
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The EnterSearchMoto recording.
    /// </summary>
    [TestModule("de180dfc-5b04-4507-bbd9-852f314f76aa", ModuleType.Recording, 1)]
    public partial class EnterSearchMoto : ITestModule
    {
        /// <summary>
        /// Holds an instance of the MobileCIDemo.MobileCIDemoRepository repository.
        /// </summary>
        public static MobileCIDemo.MobileCIDemoRepository repo = MobileCIDemo.MobileCIDemoRepository.Instance;

        static EnterSearchMoto instance = new EnterSearchMoto();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public EnterSearchMoto()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static EnterSearchMoto Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 1.5m for item 'RanorexRxBrowserMotoX.HomePage.ShoppingCartLogoMoto' to exist.", repo.RanorexRxBrowserMotoX.HomePage.ShoppingCartLogoMotoInfo, new ActionTimeout(90000), new RecordItemIndex(0));
            repo.RanorexRxBrowserMotoX.HomePage.ShoppingCartLogoMotoInfo.WaitForExists(90000);
            
            Report.Log(ReportLevel.Info, "Set Value", "Setting attribute TagValue to '204476163' on item 'RanorexRxBrowserMotoX.HomePage.SearchBoxInput'.", repo.RanorexRxBrowserMotoX.HomePage.SearchBoxInputInfo, new RecordItemIndex(1));
            repo.RanorexRxBrowserMotoX.HomePage.SearchBoxInput.Element.SetAttributeValue("TagValue", "204476163");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Touch", "Touch item 'RanorexRxBrowserMotoX.HomePage.SearchButton' at Center", repo.RanorexRxBrowserMotoX.HomePage.SearchButtonInfo, new RecordItemIndex(2));
            repo.RanorexRxBrowserMotoX.HomePage.SearchButton.Touch();
            Delay.Milliseconds(500);
            
            Report.Log(ReportLevel.Info, "Wait", "Waiting 1.5m for item 'RanorexRxBrowserMotoX.RakePage.RakeImage' to exist.", repo.RanorexRxBrowserMotoX.RakePage.RakeImageInfo, new ActionTimeout(90000), new RecordItemIndex(3));
            repo.RanorexRxBrowserMotoX.RakePage.RakeImageInfo.WaitForExists(90000);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
